<?php

require_once 'shortcodes/index.php';
require_once 'lib/location.php';
require_once 'lib/translate.php';

register_nav_menus(
  array(
    'header' => 'Header menu',
    'mobile' => 'Mobile menu',
    'footer' => 'Footer menu',
  )
);

//translation

function gett($text) {
	if(function_exists('pll__') ) {
		return pll__($text);
  }

	return $text;
}

function clean_menu($nav) {
  $menu = preg_replace( array( '#^<div[^>]*>#', '#</div>$#' ), '', $nav );
  return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );
}

add_action( 'wp_ajax_nopriv_store_comment', 'store_comment' );
add_action( 'wp_ajax_store_comment', 'store_comment' );

function store_comment() {
  $data = $_POST['data'];
  $comment_id = wp_new_comment( $data );
  $res = ['id' => $comment_id];
  header('Content-type: application/json');
	echo wp_json_encode($res);
  die();
}

function cbz_widgets_init() {
	register_sidebar( array(
		'name'          => 'sidebar post',
    'id'            => 'post_sidebar',
		'before_widget' => '<div class="post-single__sidebar-section">',
    'after_widget'  => '</div>',
    'before_title'  => '<h5 class="widget_title">',
    'after_title'   => '</h5>',
  ) );

  register_sidebar( array(
		'name'          => 'footer left',
    'id'            => 'footer_left',
		'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<h5 class="widget_title">',
    'after_title'   => '</h5>',
  ) );

  register_sidebar( array(
		'name'          => 'footer right',
    'id'            => 'footer_right',
		'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<h5 class="widget_title">',
    'after_title'   => '</h5>',
	) );
}

add_action( 'widgets_init', 'cbz_widgets_init' );


function cbz_search_form( $form ) {
  $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >
  <input type="text" value="' . get_search_query() . '" name="s" id="s" />
  <button><i class="fas fa-search"></i></button>
  </form>';

  return $form;
}

add_filter( 'get_search_form', 'cbz_search_form', 100 );