<?php

function register_translation($name ='', $text, $group = 'BS', $multiline = true) {
	if(function_exists('pll_register_string')) {

      $name = str_replace(' ', '_', strtolower(substr($text, 0, 25)));


		pll_register_string( $name, $text, $group, $multiline );
	}
}

register_translation('post_recommendation', 'También te recomendamos');
register_translation('post_share', 'Compartir en');
register_translation('post_author', 'Autor');
register_translation('post_author_bio', 'Médico General, graduado de la Universidad del Sinú sede Cartagena y especialista en Cirugía Plástica, Estética y Reconstructiva de la Pontificia Universidad Católica de Argentina en Buenos Aires.');
register_translation('post_comments', 'comentarios');
register_translation('post_comment', 'Comentario');
register_translation('post_see_more', 'Ver más');
register_translation('post_leave_a_comment', 'Dejanos un comentario');
register_translation('form_name', 'Nombre');
register_translation('form_email', 'Email');
register_translation('form_consult', 'Consultar');
register_translation('form_call', 'Llamar');
register_translation('comment_send', 'Comentario enviado');
register_translation('post_comment', 'Comentar');