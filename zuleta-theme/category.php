<?php get_header(); ?>

<?php echo do_shortcode('[header]') ?>

<div class="page-title" style="display: block; padding: 60px; background: #f1f1f1; margin-bottom: 30px">
  <div class="container">
      <h1><?php echo gett('Categoría') . " " . get_category($cat)->name; ?></h1>
  </div>
</div>

<div class="container">
  <?php echo do_shortcode("[posts categories=' " . get_category($cat)->name . "']")  ?>
</div>

<?php get_footer() ?>