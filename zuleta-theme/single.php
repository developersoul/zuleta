<?php get_header() ?>
<?php echo do_shortcode('[header]') ?>
<div class="post-single">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="container">
      <div class="row">
      <div class="col-lg-8">
      <h1><?php the_title(); ?></h1>
      <div class="post-single__meta">
          <span>
            <i class="far fa-clock"></i> <?php echo get_the_date( 'd-m-Y', $post->ID ); ?>
          </span>
          <span><i class="far fa-keyboard"></i> <?php echo get_the_author_meta('first_name', $post->post_author) . " " . get_the_author_meta('last_name', $post->post_author) ?></span>
          <span>
<i class="far fa-folder-open"></i> <?php foreach(get_the_category($post->ID) as $cat): ?> <?php echo $cat->name ?> <?php endforeach; ?>
          </span>
        </div>

        <img class="post-single__image" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full') ?>" alt="">

        <div class="post-single__content">
          <?php the_content() ?>
        </div>

        <div class="post-single__share">
          <a class="btn btn-facebook">
            <i class="fab fa-facebook-f"></i> <?php echo gett('Compartir en') ?> facebook
          </a>
          <a class="btn btn-twitter">
            <i class="fab fa-twitter"></i> <?php echo gett('Compartir en') ?> twitter
          </a>
        </div>
        <div class="post-single__author">
          <img src="<?php echo get_template_directory_uri() ?>/client/img/profile.png" alt="">
          <div class="post-single__author__info">
            <span><b><?php echo gett('Autor') ?>:</b> <?php echo get_the_author_meta('first_name', $post->post_author) . " " . get_the_author_meta('last_name', $post->post_author) ?></span>
            <span><?php echo gett('Médico General, graduado de la Universidad del Sinú sede Cartagena y especialista en Cirugía Plástica, Estética y Reconstructiva de la Pontificia Universidad Católica de Argentina en Buenos Aires.') ?></span>
          </div>
        </div>
        <div class="post-single__comment-box">
          <h3><?php echo gett('Dejanos un comentario') ?></h3>
          <?php echo do_shortcode('[comment_form]') ?>
        </div>

        <?php
          $cats = array_map(function($cat) {
            return $cat->name;
          }, get_the_category($post->ID));
          $cats = implode(',', $cats);
        ?>

        <div class="post-single__comments">
          <?php wp_list_comments(); ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="post-single__sidebar">
          <?php dynamic_sidebar('post_sidebar') ?>
        </div>
    </div>
</div>
  <section class="related-posts">
    <h3 class="related-posts__title"><?php echo gett('También te recomendamos') ?></h3>
    <?php echo do_shortcode("[posts categories='$cats']")  ?>
  </section>
  </div>

</div>

<?php endwhile; else : ?>
  <h2> <?php echo gett('404') ?> </h2>
<?php endif; ?>

<?php get_footer() ?>