import React, { Component } from 'react';

class FullHeightHero extends Component {

  goDown = () => {
    console.log(this.container.offsetHeight);
  }

  render() {
    const { bg, logo, anchor, dir_uri } = this.props;

    return (
        <div
          className="full-height-hero"
          style={{ backgroundImage: `url(${ bg})` }}
          ref={ref => this.container = ref}
        >
        <div className="container">
          {logo && <img src={logo} />}
        </div>
          <a
            href={anchor}
            className="full-height-hero__go-down"
            style={{ backgroundImage: `url(${dir_uri}/client/img/btn_down.png)` }}
            onClick={this.goDown}></a>
        </div>
    )
  }

}

export default FullHeightHero;