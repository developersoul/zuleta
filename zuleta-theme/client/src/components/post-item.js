import React from 'react';

const PostItem = (props) => {
  const { post, trans } = props;
  return (
    <div className="post-item">
      <a
        href={post.post_permalink}
        className="post-item__img"
        style={{backgroundImage: `url(${post.post_image})`}}>
      </a>

      <div className="post-item__excerpt">
        <a href={post.post_permalink}><h4>{post.post_title}</h4></a>
        <div className="post-item__excerpt__meta">
          <span>
            <i class="far fa-clock"></i> {post.post_date_formated}
          </span>
          <span>
          <i class="far fa-folder-open"></i> {post.post_categories && post.post_categories.map((cat, i) => {
            return `${cat.name}${post.post_categories.length > 1 && i + 1 < post.post_categories.length ? ', ' : '' }`
          })}
          </span>
        </div>

        <div className="post-item__excerpt__meta">
          <span><i class="far fa-keyboard"></i> {post.author_name}</span>
          <span><i class="far fa-comment"></i> {post.comment_count} {trans.comments}</span>
        </div>

        <p>{post.post_short}...</p>
        <a href={post.post_permalink} className="btn">{trans.see_more}</a>
      </div>

    </div>
  );
};

export default PostItem;