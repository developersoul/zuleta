import React, { Component } from 'react';
import PostItem from './post-item';

class Posts extends Component {

  render() {
    const { posts, trans } = this.props;

    return (
      <div className="row">
      {posts.map(post =>
        <div className="col-lg-4 col-md-4">
          <PostItem post={post} trans={trans} />
        </div>
      )}
      </div>
    )
  }
}

export default Posts;