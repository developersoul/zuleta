import React, { Component } from 'react';

class Cta extends Component {

  render() {
    const { url, text } = this.props;
    return (
      <div className="cta">
        <div className="cta__text">
          <h3>{text}</h3>
        </div>
        <a href={url} className="btn cta-btn">AGENDA TU CITA HOY MISMO</a>
      </div>
    )
  }
}

export default Cta;