import React, { Component } from 'react';

class SocialBanner extends Component {

  render() {
    return (
      <section className="social-banner">
        <a href="#" className="social-banner__item fb">
          <span><i className="ion-social-facebook"></i></span>
          Seguir en facebook
        </a>
        <a href="#" className="social-banner__item ig">
          <span><i className="ion-social-instagram-outline"></i></span>
          Seguir en instagram
        </a>
        <a href="https://wa.me/573176987268" className="social-banner__item wa">
          <span><i className="ion-social-whatsapp-outline"></i></span>
          Comunicarse en whatsapp
        </a>
      </section>
    )
  }
}

export default SocialBanner;