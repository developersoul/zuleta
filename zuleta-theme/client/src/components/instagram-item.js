import React, { Component } from 'react';
import request from 'axios';

class InstagramItem extends Component {
  state = {
    item: {}
  }

  componentDidMount() {
    request.get(`https://api.instagram.com/oembed?url=http://instagr.am/p/${this.props.id}/`)
    .then(res => {
      console.log(res);
      this.setState({ item: res.data });
    })
  }

  render() {
    return (
      <div className="instagram-item">
        <img src={this.state.item.thumbnail_url} style={{ width: '100%' }} />
      </div>
    )
  }
}

export default InstagramItem;