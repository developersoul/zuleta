import slick from 'slick-carousel';
import React, { Component } from 'react';
import jQuery from 'jquery';

class Slider extends Component {
	componentDidMount() {
		jQuery(this.slider).slick({
			autoplay: true,
      arrows: true,
      nextArrow: this.next,
      prevArrow: this.prev
		});
	}

	render() {
		const { slides } = this.props;

		return (
			<section className="slider">
				<div ref={ref => this.slider = ref}>
					{slides.map((slide, i) => {
						return <div key={i} >
              <img src={slide.image}/>
						</div>
					})}
        </div>
				<button className="slider__btn slider__btn-next" ref={ref => this.prev = ref}>
					<i className="fas fa-chevron-right"></i>
				</button>
				<button className="slider__btn slider__btn-prev" ref={ref => this.next = ref}>
				<i className="fas fa-chevron-left"></i>
				</button>
			</section>
		);
	}
}

export default Slider;