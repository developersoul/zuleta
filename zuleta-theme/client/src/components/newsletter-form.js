import React, { Component } from 'react';
import { validateEmail, validateEmpty } from '../lib/validate';

class NewsletterForm extends Component {
  state = {
    email: '',
    success: false,
    invalid: false
  }

  handleSubmit = (e) => {
    if(e) e.preventDefault();

    if(validateEmail(this.state.email)) {
      this.setState({ email: '', invalid: false, success: true });
    } else {
      this.setState({ invalid: true, success: false });
    }
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  render() {
    const { success, invalid } = this.state;

    return (
      <form onSubmit={this.handleSubmit} className="newsletter-form">
        <div className="form-group">
          <input
            placeholder="Email"
            name="email"
            type="text"
            className="form-control"
            onChange={this.handleChange}
            value={this.state.email}
          />
        </div>
        {invalid &&
          <div className="form-group">
            <div className="alert alert-danger">Email invalido</div>
          </div>
        }
        {success &&
          <div className="form-group">
            <div className="alert alert-success">Subscrito</div>
          </div>
        }
        <div className="form-group">
          <button className="btn">SUSCRÍBETE</button>
        </div>
      </form>
    )
  }
}

export default NewsletterForm;