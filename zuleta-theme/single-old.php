<?php get_header() ?>
<?php echo do_shortcode('[header]') ?>
<div class="post-single">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <div
    class="post-single__header"
    style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'full') ?>); background-attachment: fixed"
  >
    <div class="container">
      <div class="post-single__header_overlay"></div>
      <div class="post-single__header_content">
        <h1><?php the_title(); ?></h1>
        <h4 class="post-single__date"><?php echo get_the_date( 'd-m-Y', $post->ID ); ?></h4>
      </div>
    </div>
  </div>
  <div class="container post-single__container">
    <?php the_content() ?>
  </div>
</div>

<?php endwhile; else : ?>
  <h2> <?php echo gett('404') ?> </h2>
<?php endif; ?>

<?php get_footer() ?>