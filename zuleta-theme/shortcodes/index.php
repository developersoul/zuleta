<?php
//SHORTCODES
require_once 'full-height-hero.php';
require_once 'header.php';
require_once 'form.php';
require_once 'contact-map.php';
require_once 'instagram-item.php';
require_once 'procedure-item.php';
require_once 'posts.php';
require_once 'slider.php';
require_once 'yt-slider.php';
require_once 'procedure-content.php';
require_once 'newsletter-form.php';
require_once 'cta.php';
require_once 'testimonial.php';
require_once 'comment_form.php';

//VISUALCOMPOSER
require_once 'visual-composer/full-height-hero.php';
require_once 'visual-composer/header.php';
require_once 'visual-composer/form.php';
require_once 'visual-composer/contact-map.php';
require_once 'visual-composer/instagram-item.php';
require_once 'visual-composer/procedure-item.php';
require_once 'visual-composer/posts.php';
require_once 'visual-composer/slider.php';
require_once 'visual-composer/yt-slider.php';
require_once 'visual-composer/procedure-content.php';
require_once 'visual-composer/cta.php';
require_once 'visual-composer/testimonial.php';
