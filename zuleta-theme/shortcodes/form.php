<?php

include_once str_replace('shortcodes', '', __DIR__) . 'lib/countries.php';
include_once str_replace('shortcodes', '', __DIR__) . 'lib/location.php';

function form_sc( $atts ){

	$at = shortcode_atts([
		'select_country' => gett('Seleccionar país'),
		'city' => gett('Ciudad'),
		'name' => gett('Nombre'),
		'lastname' => gett('Apellido'),
		'email' => gett('Email'),
		'btn' => gett('Consultar')
	], $atts);

	$props = [
		"countries" => getCountries(),
		"country" => getCountry(),
    "trans" => [
      "name" => $at['name'],
			"email" => $at['email'],
			"select_country" => $at['select_country'],
			"city" => $at['city'],
      "btn" => $at['btn'],
    ]
	];

	ob_start();
	?>

	<div
    class="form-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php

	return ob_get_clean();
};

add_shortcode( 'form', 'form_sc' );