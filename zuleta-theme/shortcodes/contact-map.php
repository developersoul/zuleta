<?php

function contact_map_sc( $atts ) {

	$at = shortcode_atts([
		'city' => '',
		'address' => '',
    'phone' => '',
    'cta_text' => gett('Llamar'),
    'lat' => '',
    'lng' => ''
	], $atts);

	$props = [
    'city' => $at['city'],
		'address' => $at['address'],
    'phone' => $at['phone'],
    "center" => [
      "lat" => $at['lat'],
      "lng" => $at['lng'],
    ],
    "trans" => [
      "cta_text" => $at['cta_text'],
    ]
	];

	ob_start();
	?>

	<div
    class="contact-map-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php

	return ob_get_clean();
};

add_shortcode( 'contact_map', 'contact_map_sc' );