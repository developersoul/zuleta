<?php

function instagram_item_sc( $atts ){

	$at = shortcode_atts([
		'id' => '',
	], $atts);

	$props = [
		"id" => $at['id']
	];

	ob_start();
	?>

	<div
    class="instagram-item-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php

	return ob_get_clean();
};

add_shortcode( 'instagram_item', 'instagram_item_sc' );