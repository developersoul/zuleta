<?php

function header_vc() {
	$params = [
		[
			'type' => 'attach_image',
			'heading' => 'Logo',
			'param_name' => 'logo',
		]
	];

	vc_map(
    [
      "name" =>  "Header",
      "base" => "header",
      "category" =>  "CZB",
      'params' => $params
		]
	);
}

add_action( 'vc_before_init', 'header_vc' );