<?php

function yt_slider_vc() {

	$subparams = [
		[
			"type" => "textfield",
			"heading" => "youtube id",
			"param_name" => "yt_id"
    ], [
      "type" => "textfield",
			"heading" => "título",
			"param_name" => "title"
    ]
	];

	$params = [
    [
      'type' => 'param_group',
      'value' => '',
      'param_name' => 'slide',
      'params' => $subparams
    ]
	];

  vc_map(
    array(
      "name" =>  "Youtube Slider",
      "base" => "yt_slider",
      "category" =>  "CZB",
      "params" => $params
    )
  );
};

add_action( 'vc_before_init', 'yt_slider_vc' );