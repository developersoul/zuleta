<?php

function testimonial_vc() {

	$params = [
    [
			"type" => "attach_image",
			"heading" => "Image",
			"param_name" => "image"
    ],
    [
			'type' => 'textarea',
			'heading' => 'text',
			'param_name' => 'text',
			'value' => ''
    ],
    [
			'type' => 'textfield',
			'heading' => 'text',
			'param_name' => 'author',
			'value' => ''
		]
	];

  vc_map(
    array(
      "name" =>  "Testimonial",
      "base" => "testimonial",
      "category" =>  "CZB",
      "params" => $params
    )
  );
};

add_action( 'vc_before_init', 'testimonial_vc' );