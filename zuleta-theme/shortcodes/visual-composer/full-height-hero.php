<?php

function full_height_hero_vc() {
	$params = [
		[
			'type' => 'attach_image',
			'heading' => 'Background',
			'param_name' => 'bg',
		],
		[
			'type' => 'attach_image',
			'heading' => 'Logo',
			'param_name' => 'logo',
		],
		[
			'type' => 'textfield',
			'heading' => 'Anchor',
			'param_name' => 'anchor',
		]
	];

	vc_map(
    [
      "name" =>  "Full height hero",
      "base" => "full_height_hero",
      "category" =>  "CZB",
      'params' => $params
		]
	);
}

add_action( 'vc_before_init', 'full_height_hero_vc' );