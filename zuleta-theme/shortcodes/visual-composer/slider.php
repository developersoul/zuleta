<?php

function slider_vc() {

	$subparams = [
		[
			"type" => "attach_image",
			"heading" => "Image",
			"param_name" => "image"
		]
	];

	$params = [
    [
      'type' => 'param_group',
      'value' => '',
      'param_name' => 'slide',
      'params' => $subparams
    ]
	];

  vc_map(
    array(
      "name" =>  "Slider",
      "base" => "slider",
      "category" =>  "CZB",
      "params" => $params
    )
  );
};

add_action( 'vc_before_init', 'slider_vc' );