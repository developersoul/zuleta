<?php

function hero_vc() {
	$params = [
		[
			'type' => 'attach_image',
			'heading' => 'Left image',
			'param_name' => 'bg',
		]
	];

	vc_map(
    [
      "name" =>  "Hero",
      "base" => "hero",
      "category" =>  "CZB",
      'params' => $params
		]
	);
}

add_action( 'vc_before_init', 'hero_vc' );