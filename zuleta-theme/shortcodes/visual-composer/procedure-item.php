<?php

function procedure_item_vc() {

	$categories = [
		[
			'type' => 'textfield',
			'heading' => 'Category name',
			'param_name' => 'category_name',
			'value' => ''
		],
		[
			'type' => 'textarea',
			'heading' => 'category text',
			'param_name' => 'category_text',
			'value' => ''
		]
	];

	$params = [
		[
			'type' => 'textfield',
			'heading' => 'Title',
			'param_name' => 'title',
		],
		[
			'type' => 'textfield',
			'heading' => 'Url',
			'param_name' => 'url',
    ],
    [
			'type' => 'attach_image',
			'heading' => 'Image',
			'param_name' => 'image_url',
		],
		[
			'type' => 'param_group',
			'heading' => 'categories',
			'param_name' => 'categories',
			'params' => $categories
		]
	];

	vc_map(
    [
      "name" =>  "Procedure item",
      "base" => "procedure_item",
      "category" =>  "CZB",
      'params' => $params
		]
	);
}

add_action( 'vc_before_init', 'procedure_item_vc' );