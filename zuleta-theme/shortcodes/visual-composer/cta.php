<?php

function cta_vc() {

	$params = [
    [
			'type' => 'textfield',
			'heading' => 'Copy',
			'param_name' => 'text',
    ],
    [
			'type' => 'textfield',
			'heading' => 'url',
			'param_name' => 'url',
    ],
	];

  vc_map(
    array(
      "name" =>  "call to action",
      "base" => "cta",
      "category" =>  "CZB",
      "params" => $params
    )
  );
};

add_action( 'vc_before_init', 'cta_vc' );