<?php

function testimonial_sc( $atts ){
	$at = shortcode_atts([
    'image' => '',
    'text' => '',
    'author' => ''
	], $atts);

  $imgUrl = wp_get_attachment_url($at['image']);

	$props = [
    'imgUrl' => $imgUrl,
    'text' => $at['text'],
    'author' => $at['author']
  ];

	ob_start();
	?>
    <section
    	class="testimonial-container"
    	data-props='<?php echo wp_json_encode($props) ?>'
    ></section>

	<?php

	return ob_get_clean();
}

add_shortcode( 'testimonial', 'testimonial_sc' );