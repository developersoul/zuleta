<?php

function cta_sc( $atts ){

	$at = shortcode_atts([
    'text' => '',
    'url' => ''
	], $atts);

	$props = [
		"text" => $at['text'],
		"url" => $at['url'],
	];

	ob_start();
	?>

	<div
    class="cta-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php

	return ob_get_clean();
};

add_shortcode( 'cta', 'cta_sc' );