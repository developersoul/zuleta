<?php

function full_height_hero_sc( $atts ){

	$at = shortcode_atts([
		'bg' => '',
		'anchor' => '#',
		'logo' => ''
	], $atts);

	$props = [
		"bg" => wp_get_attachment_url($at['bg']),
		"logo" => wp_get_attachment_url($at['logo']),
		"dir_uri" => get_template_directory_uri(),
		"anchor" => $at['anchor'],
	];

	ob_start();
	?>

	<div
    class="full-height-hero-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php

	return ob_get_clean();
};

add_shortcode( 'full_height_hero', 'full_height_hero_sc' );