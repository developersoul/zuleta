<?php

function yt_slider_sc( $atts ){
	$at = shortcode_atts([
    'slide' => ''
	], $atts);


	$props = [
		'slides' => vc_param_group_parse_atts($at['slide']),
		"dir_uri" => get_template_directory_uri(),
	];

	ob_start();
	?>
    <section
		class="yt-slider-container"
    	data-props='<?php echo wp_json_encode($props) ?>'
    ></section>

	<?php

	return ob_get_clean();
}

add_shortcode( 'yt_slider', 'yt_slider_sc' );